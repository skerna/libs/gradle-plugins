plugins {
    id("io.skerna.libs.gradle.base")
}

packageSpec{
//    this.multiplatform{
//        enableAndroid = true
//    }
    metadata{
        description("Demo description library")
        developers{
            developer("robotdev")
        }
        licences{
            licence("MIT")
        }
    }
}