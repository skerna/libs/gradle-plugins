object PluginBaseCoordinates {
    const val ID = "io.skerna.libs.gradle.base"
    const val GROUP = "io.skerna.libs.gradle"
    const val VERSION = "1.0.0"
    const val IMPLEMENTATION_CLASS = "io.skerna.libs.gradle.base.SkernaPluginBase"
    object PluginBundle {
        const val VCS = "https://github.com/cortinico/kotlin-gradle-plugin-template"
        const val WEBSITE = "https://github.com/cortinico/kotlin-gradle-plugin-template"
        const val DESCRIPTION = "An empty Gradle plugin created from a template"
        const val DISPLAY_NAME = "An empty Gradle Plugin from a template"
        val TAGS = listOf(
            "plugin",
            "gradle",
            "sample",
            "template"
        )
    }
}

object PluginMultplatformCoordinates {
    const val ID = "io.skerna.libs.gradle.multiplatform"
    const val GROUP = "io.skerna.libs.gradle"
    const val VERSION = "1.0.0"
    const val IMPLEMENTATION_CLASS = "io.skerna.mlibs.gradle.MultiplatformPlugin"
    object PluginBundle {
        const val VCS = "https://github.com/cortinico/kotlin-gradle-plugin-template"
        const val WEBSITE = "https://github.com/cortinico/kotlin-gradle-plugin-template"
        const val DESCRIPTION = "An empty Gradle plugin created from a template"
        const val DISPLAY_NAME = "An empty Gradle Plugin from a template"
        val TAGS = listOf(
            "plugin",
            "gradle",
            "sample",
            "template"
        )
    }
}




