fun findProperty(s: String) = project.findProperty(s) as String?
private val Project.bintray: com.jfrog.bintray.gradle.BintrayExtension
    get() = extensions.findByName("bintray") as? com.jfrog.bintray.gradle.BintrayExtension ?: error("Not found bvintray extensoins $name")

bintray {
    user = findProperty("BINTRAY_USER")
    key = findProperty("BINTRAY_KEY")
    publish = true
    setPublications(publicationName)
    pkg.run {
        repo = "maven"
        name = project.name
        userOrg = "skerna"
        override = true
        dryRun = false
        websiteUrl = "https://gitlab.com/skerna/libs/gradle-plugins"
        vcsUrl = "https://gitlab.com/skerna/libs/gradle-plugins"
        description = "Gradle plugins for skerna devs"
        setLabels("kotlin")
        setLicenses("MIT")
        desc = description
    }
}