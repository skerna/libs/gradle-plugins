package io.skerna.libs.gradle.base.actions

import com.jfrog.bintray.gradle.BintrayExtension
import com.jfrog.bintray.gradle.tasks.BintrayUploadTask
import io.skerna.libs.gradle.base.Utils
import io.skerna.libs.gradle.base.extensions.BaseExtension
import org.gradle.api.Project
import org.gradle.api.publish.PublishingExtension
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.bundling.Jar

/**
 * rexcode-multiplatform-gradle
 * @author ronald
 **/
class ConfigurePublish(val project: Project, val extension: BaseExtension) {
    private val Project.bintray: BintrayExtension
        get() = extensions.findByName("bintray") as? BintrayExtension ?: error("Not found bvintray extensoins $name")

    private val Project.publishing: PublishingExtension
        get() = extensions.findByName("publishing") as? PublishingExtension
            ?: error("Not found publishing  extensoins $name")

    private val Project.sourceSets: SourceSetContainer
        get() = extensions.findByName("sourceSets") as? SourceSetContainer
            ?: error("Not found sourceSets extensoins $name")

    private val logger = project.logger;

    fun apply() {
        logger.trace("Aplicando configuracion para publicar librerias")

        val hasJvmPlugin = project.pluginManager.hasPlugin("java-library")
        val haskotlinMp = project.pluginManager.hasPlugin("org.jetbrains.kotlin.multiplatform")
        val haskotlinJvm = project.pluginManager.hasPlugin("org.jetbrains.kotlin.jvm")
        if(!hasJvmPlugin && !haskotlinMp && !haskotlinJvm){
            logger.info("El proyecto no tiene ningun soporte para publicar, saltando.")
            return;
        }
        val metadata = extension.metadataExtension
        with(project) {
            applyPublicationsPlugins()
        }

        project.afterEvaluate {
            with(project) {
                val sourceJar = project.tasks.create("source", Jar::class.java).apply {
                    archiveClassifier.set("sources")
                    from(sourceSets.getByName("main").allSource)
                }
                publishing.publications {
                    val publication: MavenPublication = it.create("lib", MavenPublication::class.java)
                    publication.groupId = project.group.toString()
                    publication.artifactId = project.name
                    publication.version = project.version.toString()
                    publication.from(project.components.findByName("java"))
                    publication.pom { pom ->
                        pom.withXml { xml ->
                            xml.asNode().apply {
                                appendNode("description", metadata.description)
                                appendNode("name", rootProject.name)
                                appendNode("url", metadata.versionControlRepo)
                                appendNode("developers").apply {
                                    metadata.developers.developers.forEach { developer ->
                                        appendNode("developer").apply {
                                            appendNode("id", developer.email)
                                            appendNode("name", developer.name)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    publication.artifact(sourceJar)
                    // publication.artifact(dokkaJar)
                }


                bintrayConfig()
                val uploadTask = project.tasks.findByPath("bintrayUpload") as BintrayUploadTask
                uploadTask.dependsOn("publishToMavenLocal")
                uploadTask.doFirst {
                    val task = it as BintrayUploadTask
                    val publications: List<String> =
                        project.publishing.publications.filter { publication -> !publication.name.contains("-test") }
                            .map { pub -> pub.name }
                    task.setPublications(*publications.toTypedArray())
                }
            }
        }
    }

    private fun Project.applyPublicationsPlugins() {
        plugins.run {
            apply("maven-publish")
            apply("com.jfrog.bintray")
        }
    }

    private fun Project.bintrayConfig() {
        val metadata = extension.metadataExtension
        val gitUrl = metadata.versionControlRepo
        val useOrg = metadata.organization
        val description = metadata.description
        val siteUrl = metadata.websiteUrl
        val listLabels = metadata.labels
        if(listLabels.isEmpty()){
            listLabels.add("library")
        }

        var userBintray = Utils.findVarialbe(project,"BINTRAY_USER")
        var keyBintray = Utils.findVarialbe(project,"BINTRAY_KEY")
        val licences = metadata.licences.licences.map { it.name }.toTypedArray()
        val labels = listLabels.map { it }.toTypedArray()
        val issueTrack = metadata.issueTrackerUrl
        if (userBintray == null) {
            userBintray = ""
        }
        if (keyBintray == null) {
            keyBintray = ""
        }
        bintray.apply {
            user = userBintray
            key = keyBintray
            override = true // for multi-platform Kotlin/Native publishing
            setConfigurations("archives")
            dryRun = false
            pkg.name = project.name
            pkg.setLabels(*labels)
            pkg.repo = "maven"
            pkg.userOrg = useOrg
            pkg.desc = description
            pkg.websiteUrl = siteUrl
            pkg.vcsUrl = gitUrl
            pkg.issueTrackerUrl = issueTrack
            pkg.publicDownloadNumbers = true
            pkg.version.desc = description
            pkg.setLicenses(*licences)
        }
    }
}