package io.skerna.libs.gradle.base.actions

import io.skerna.libs.gradle.base.extensions.BaseExtension
import org.gradle.api.Project

/**
 * arch-gradle
 * @author ronald
 **/
class ConfigurePlugins constructor(val project:Project,val baseExtension: BaseExtension) {
    fun apply(){
        val module = baseExtension.moduleExtension
        project.plugins.run {
            apply("base")
            apply("maven-publish")
        }
    }
}