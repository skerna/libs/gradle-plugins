package io.skerna.libs.gradle.base.actions

import io.skerna.libs.gradle.base.extensions.BaseExtension
import org.gradle.api.Project

/**
 * arch-gradle
 * @author ronald
 **/
class ConfigureRepos constructor(val project:Project, val baseExtension: BaseExtension) {
    fun apply(){
        with(project){
            repositories.google()
            repositories.mavenCentral()
            repositories.mavenLocal()
            repositories.maven {
                it.setUrl("https://dl.bintray.com/rexcode/maven")
            }
        }
    }
}