package io.skerna.libs.gradle.base

/**
 * arch-gradle
 * @author ronald
 **/
object Constants {
    const val EXTENSION_NAME = "packageSpec"
    const val EXTENSION_MODULE = "module"
    const val EXTENSION_ENVIRONMENT = "environment"
    const val EXTENSION_RESOURCE = "resources"
    const val TASK_GENERATE_RESOURCES = "generateResources"
    const val TASK_GENERATE_TREE = "generateTree"
    const val TASKS_GROUP = "skerna"
}