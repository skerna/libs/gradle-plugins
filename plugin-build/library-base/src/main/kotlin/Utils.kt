package io.skerna.libs.gradle.base

import org.gradle.api.Project

/**
 * gradle-plugins
 * @author ronald
 **/
object Utils {
    /**
     * findVarialbe retorna una variable buscando primero como propiedad del proyecto sino la encuentra
     * retorna busca en las vairables de entorno
     */
    fun findVarialbe(project:Project,name:String): String? {
        var value = project.property(name)?.toString()
        if(value == null){
            value = System.getenv(name)
        }

        return value
    }
}