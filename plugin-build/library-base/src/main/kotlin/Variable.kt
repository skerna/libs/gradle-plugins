package io.skerna.libs.gradle.base

import java.io.Serializable
import java.lang.reflect.Type

data class Variable(val value: String, val placeholder: Boolean, val type: Type) : Serializable