package io.skerna.libs.gradle.base

import io.skerna.libs.gradle.base.Constants.EXTENSION_ENVIRONMENT
import io.skerna.libs.gradle.base.Constants.EXTENSION_NAME
import io.skerna.libs.gradle.base.Constants.TASK_GENERATE_RESOURCES
import io.skerna.libs.gradle.base.actions.ConfigurePlugins
import io.skerna.libs.gradle.base.actions.ConfigurePublish
import io.skerna.libs.gradle.base.actions.ConfigureRepos
import io.skerna.libs.gradle.base.extensions.BaseExtension
import io.skerna.libs.gradle.base.extensions.EnvironmentExtension
import org.gradle.api.Plugin
import org.gradle.api.Project


open class SkernaPluginBase : Plugin<Project> {


    final override fun apply(project: Project) {
        val primaryExtension = configureMainExtension()
        val baseExtesion: BaseExtension =
            project.extensions.create(EXTENSION_NAME, primaryExtension, project)
        val envExtension: EnvironmentExtension =
            project.extensions.create(EXTENSION_ENVIRONMENT, EnvironmentExtension::class.java, project)

        val processResources = project.tasks.findByName("processResources")
        val generateResourceTask = project.tasks.create(TASK_GENERATE_RESOURCES, ResourcesTask::class.java)
        processResources?.let { generateResourceTask.dependsOn(it) }

        val pluginAction = ConfigurePlugins(project, baseExtesion)
        val reposAction = ConfigureRepos(project, baseExtesion)
        val publish = ConfigurePublish(project, baseExtesion)
        pluginAction.apply()
        reposAction.apply()
        publish.apply()
        project.afterEvaluate { project->
            configurePlugin(project)
        }
    }

    open fun configureMainExtension(): Class<out BaseExtension> {
        return BaseExtension::class.java
    }

    open fun configurePlugin(project: Project){

    }
}
