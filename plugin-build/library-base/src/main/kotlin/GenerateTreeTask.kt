package io.skerna.libs.gradle.base

import io.skerna.libs.gradle.base.Constants.TASKS_GROUP
import io.skerna.libs.gradle.base.resources.PropertiesContext
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import java.nio.file.Paths

open class GenerateTreeTask : DefaultTask() {
    val CONFIG_FILE = "${project.configDirectoryPath()}/config.json"
    val LOG4J2_FILE = "${project.configDirectoryPath()}/log4j2.xml"
    val CLUSTER_FILE = "${project.configDirectoryPath()}/cluster.xml"

    @TaskAction
    fun compileTemplate() {
        val config = Paths.get(CONFIG_FILE).toFile()
        val log4j = Paths.get(LOG4J2_FILE).toFile()
        val cluster = Paths.get(CLUSTER_FILE).toFile()
        if(!config.exists()){
            config.mkdirs()
        }
        if(!log4j.exists()){
            log4j.mkdirs()
        }
        if(!cluster.exists()){
            cluster.mkdirs()
        }
    }

    init {
        setGroup(TASKS_GROUP)
    }
}