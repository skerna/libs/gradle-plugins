package io.skerna.libs.gradle.base.resources

import java.nio.file.Path

/**
 * ContextReader
 */
interface ContextReader {
    fun createContext(contextConfiguration: Path): Any
}