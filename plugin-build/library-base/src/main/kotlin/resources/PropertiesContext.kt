package io.skerna.libs.gradle.base.resources

import org.gradle.api.logging.Logging
import java.io.FileInputStream
import java.io.IOException
import java.nio.file.Path
import java.util.*

/**
 * PropertiesContext
 */
class PropertiesContext : ContextReader {
    override fun createContext(contextConfiguration: Path): Any {
        LOG.info("LOAD DEFSPROPS from {}", contextConfiguration.toAbsolutePath())
        try {
            FileInputStream(contextConfiguration.toFile()).use { input ->
                val prop = Properties()

                // load a properties file
                prop.load(input)
                LOG.info("DEFSPROPS from {}", prop)
                return prop
            }
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
        return HashMap<Any, Any>()
    }

    companion object {
        private val LOG = Logging.getLogger(PropertiesContext::class.java)
    }
}