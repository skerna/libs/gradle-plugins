package io.skerna.libs.gradle.base.resources

import io.skerna.libs.gradle.base.configDirectory
import io.skerna.libs.gradle.base.configDirectoryPath
import io.skerna.libs.gradle.base.configOutDirectory
import io.skerna.libs.gradle.base.configOutDirectoryPath
import org.gradle.api.Action
import org.gradle.api.Project
import java.nio.file.Path
import java.util.ArrayList
import javax.inject.Inject

open class ResourceExtension @Inject constructor(val project: Project) {
    private val resourceTemplates: MutableList<ResourceTemplate> = ArrayList<ResourceTemplate>()

    var inputDirectory: Path = project.configDirectoryPath()
    var extension = ".txt"
    var useSameExtension = true;
    var outputDirectory:String = project.configOutDirectory()
    var attributes: Map<String, Any> = project.properties as Map<String, Any>

    fun includeResource(source: String, target: String, context: Map<String, Any>, extension: String) {
        resourceTemplates.add(ResourceTemplate(source, target, context, extension))
    }

    fun getResourceDefinitions() = resourceTemplates

}