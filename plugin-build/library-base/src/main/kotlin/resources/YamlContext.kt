package io.skerna.libs.gradle.base.resources

import org.gradle.api.logging.Logging
import org.gradle.internal.impldep.org.yaml.snakeyaml.Yaml
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStreamReader
import java.io.Reader
import java.nio.file.Path

/**
 * YamlContext
 */
class YamlContext : ContextReader {
    override fun createContext(contextConfiguration: Path): Any {
        val yaml = Yaml()
        return try {
            val reader: Reader = InputStreamReader(FileInputStream(contextConfiguration.toFile()))
            yaml.load(reader)
        } catch (e: IOException) {
            LOG.error("Error during context creation")
            throw RuntimeException(e)
        }
    }

    companion object {
        private val LOG = Logging.getLogger(YamlContext::class.java)
    }
}