package io.skerna.libs.gradle.base.resources

import java.net.URI
import java.nio.file.Paths

class ResourceTemplate {
    var source: String? = null
    var target: String? = null
    var context: String? = null
    var extension: String? = null
        private set
    var contextMap: Map<String, Any>? = null

    constructor(source: String?, target: String?) {
        this.source = source
        this.target = target
    }

    constructor(source: URI, target: URI) {
        this.source = source.path
        this.target = target.path
    }

    constructor(source: String?, target: String?, context: String?) {
        this.source = source
        this.target = target
        this.context = context
    }

    constructor(source: URI, target: URI, context: String?) {
        this.source = source.path
        this.target = target.path
        this.context = context
    }

    constructor(source: String?, target: String?, context: String?, extension: String?) {
        this.source = source
        this.target = target
        this.context = context
        this.extension = extension
    }

    constructor(source: String, target: String, context: Map<String, Any>, extension: String) {
        this.source = source
        this.target = target
        this.context = null
        this.extension = extension
        contextMap = context
    }
    constructor(source: String, target: String, context: Map<String, Any>) {
        this.source = source
        this.target = target
        this.context = null
        contextMap = context
    }

    val isSourceDirectory: Boolean
        get() = Paths.get(source).toFile().isDirectory

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ResourceTemplate

        if (source != other.source) return false

        return true
    }

    override fun hashCode(): Int {
        return source?.hashCode() ?: 0
    }


}