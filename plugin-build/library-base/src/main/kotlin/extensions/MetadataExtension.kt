package io.skerna.libs.gradle.base.extensions

import org.gradle.api.Action
import org.gradle.api.Project
import javax.inject.Inject

/**
 * arch-gradle
 * @author ronald
 **/
class MetadataExtension  @Inject constructor(project: Project)  {
    internal var variables: Map<String, String>? = null
    internal var organization: String = ""
    internal var description: String = ""
    internal var versionControlRepo: String = ""
    internal var issueTrackerUrl: String = "$versionControlRepo-/issues"
    internal var websiteUrl: String = ""
    internal var licences: LicenceExtension = LicenceExtension()
    internal var developers: DeveloperExtension = DeveloperExtension()
    internal var labels: MutableList<String> = mutableListOf<String>()

    fun variables(variables: Map<String, String>) {
        this.variables = variables
    }

    fun organization(organization: String) {
        this.organization = organization
    }

    fun description(description: String) {
        this.description = description
    }

    fun versionControlRepository(versionControlRepo: String) {
        this.versionControlRepo = versionControlRepo;
    }

    fun issueTrackerUrl(issueTrackerUrl: String) {
        this.issueTrackerUrl = issueTrackerUrl
    }

    fun websiteUrl(websiteUrl: String) {
        this.websiteUrl = websiteUrl
    }

    fun licences(action: Action<LicenceExtension>) {
        action.execute(licences)
    }

    fun developers(action: Action<DeveloperExtension>) {
        action.execute(developers)
    }

    fun withLabels(vararg labels:String){
        this.labels.addAll(labels)
    }
}