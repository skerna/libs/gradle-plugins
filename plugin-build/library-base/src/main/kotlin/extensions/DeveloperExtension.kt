package io.skerna.libs.gradle.base.extensions


open class DeveloperExtension {
    internal val developers: MutableList<Developer> = mutableListOf()

    fun developer(name: String, email: String = "") {
        developers.add(Developer(name, email))
    }
    fun developer(name: String) {
        developers.add(Developer(name, ""))
    }

    override fun toString(): String {
        return "Developers(" +
                "developers=$developers" +
                ")"
    }
    data class Developer(val name: String, val email: String = "")
}