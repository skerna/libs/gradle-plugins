package io.skerna.libs.gradle.base.extensions

open class LicenceExtension {
    internal val licences: MutableList<Licence> = mutableListOf()

    fun licence(name: String) {
        licences.add(Licence(name, ""))
    }

    fun licence(name: String, url: String) {
        licences.add(Licence(name, url))
    }

    override fun toString(): String {
        return "Licences(" +
                "licences=$licences" +
                ")"
    }

    data class Licence(val name: String, val url: String)
}