package io.skerna.libs.gradle.base.extensions

import io.skerna.libs.gradle.base.resources.ResourceExtension
import org.gradle.api.Action
import org.gradle.api.Project
import javax.inject.Inject

/**
 * arch-gradle
 * @author ronald
 **/
open class BaseExtension @Inject constructor(val project: Project) {
    internal var moduleExtension: ModuleExtension = ModuleExtension(project)
    internal var metadataExtension: MetadataExtension = MetadataExtension(project)
    internal var resourceExtension: ResourceExtension = ResourceExtension(project)

    fun library(action: Action<ModuleExtension>) {
        action.execute(moduleExtension)
    }

    fun metadata(action: Action<MetadataExtension>) {
        action.execute(metadataExtension)
    }

    fun resourcesTemplates(action: Action<ResourceExtension>) {
        action.execute(resourceExtension)
    }

}