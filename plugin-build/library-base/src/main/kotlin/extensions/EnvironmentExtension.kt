package io.skerna.libs.gradle.base.extensions

import org.gradle.api.Action
import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.Project
import javax.inject.Inject

/**
 * arch-gradle
 * @author ronald
 **/
open class EnvironmentExtension @Inject constructor(val project: Project) {
    internal var entryExtensionNamed: NamedDomainObjectContainer<EnvironmentEntryExtension> =
        project.container(EnvironmentEntryExtension::class.java)
    internal var defaultEnvironment: String = getDefaultEnv()

    fun defaultEnvironment(environment: String) {
        this.defaultEnvironment = environment
    }

    fun env(name: String, action: Action<EnvironmentEntryExtension>) {
        env(name, null, action)
    }

    fun env(name: String, parent: String? = null, action: Action<EnvironmentEntryExtension>) {
        val entry = entryExtensionNamed.create(name)
        entry.inheritsFrom = parent
        if (parent != null && parent.isNotEmpty()) {
            val parentExtension = entryExtensionNamed.findByName(parent)
            parentExtension?.let { parentEnv ->
                entry.variables.putAll(parentEnv.variables)
            }
        }
        action.execute(entry)
    }

    fun environments(): NamedDomainObjectContainer<EnvironmentEntryExtension> {
        return entryExtensionNamed
    }

    fun currentEnvironment(): EnvironmentEntryExtension? {
        val enviroment = environments().filter { it.name == defaultEnvironment }.firstOrNull()

        if (enviroment == null) {
            System.err.println("Environment not cofingurared")
        }
        return enviroment
    }

    private fun getDefaultEnv(): String {
        var defaultEnv: String? = null
        if (project.hasProperty("env")) {
            defaultEnv = project.property("env") as String?
        }
        if (defaultEnv == null && entryExtensionNamed.isNotEmpty()) {
            defaultEnv = entryExtensionNamed.first().name
        }
        if (defaultEnv == null) {
            defaultEnv = "dev"
        }
        return defaultEnv
    }
}