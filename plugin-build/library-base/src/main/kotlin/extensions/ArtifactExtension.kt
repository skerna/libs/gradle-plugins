package io.skerna.libs.gradle.base.extensions

import org.gradle.api.Project

/**
 * arch-gradle
 * @author ronald
 **/
open class ArtifactExtension(val project: Project) {
    internal var versionCode = -1
    internal var versionName = project.version.toString()
    internal var codeName = project.name.toString()

    fun versionCode(versionCode: Int) {
        this.versionCode = versionCode
    }

    fun versionName(versionName: String) {
        this.versionName = versionName
    }

    fun codeName(codeName: String) {
        this.codeName = codeName
    }

    var configTemplatesDir = "${project.projectDir}/config"
    internal var enableBundle: Boolean = false

    fun configTemplatesDir(configTemplateDir: String) {
        this.configTemplatesDir = configTemplatesDir
    }

    fun enableBunlde(enableBundle: Boolean) {
        this.enableBundle = enableBundle
    }
}