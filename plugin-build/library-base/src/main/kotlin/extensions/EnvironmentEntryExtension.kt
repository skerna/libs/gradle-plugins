package io.skerna.libs.gradle.base.extensions

import io.skerna.libs.gradle.base.Variable
import java.lang.reflect.Type
import javax.inject.Inject

open class EnvironmentEntryExtension @Inject constructor(val name:String){

    val variables = HashMap<String, Variable>()

    var inheritsFrom:String?=null

    var alias = ""

    fun variable(name: String, value: String) {
        variable(name, value, "String", false)
    }
    fun variable(name: String, value: Int) {
        variable(name, value.toString(), "Int", false)
    }

    fun variable(name: String, value: String, placeholder: Boolean) {
        variable(name, value, "String", placeholder)
    }

    fun variable(name: String, value: String, type: String) {
        variable(name, value, type, false)
    }

    fun variable(name: String, value: String, type: String, placeholder: Boolean) {
        val fixedType = when {
            "Int".equals(type, true) -> Int::class.java
            "Boolean".equals(type, true) -> Boolean::class.java
            else -> String::class.java
        }
        variables[name] = Variable(value, placeholder, fixedType)
    }

    fun inheritsFrom(inheritsFrom:String){
        this.inheritsFrom = inheritsFrom
    }

    internal fun getEnvironmentArgs(reference: EnvironmentEntryExtension? = null): String {
        return reference?.variables?.let { v ->
            v.entries.joinToString(", ") { entry ->
                variables[entry.key]?.let { getValueByType(it.value, entry.value.type) }
                    ?: entry.value.let { getValueByType(it.value, it.type) }
            }
        } ?: variables.values.joinToString(", ") { getValueByType(it.value, it.type) }
    }

    private fun getValueByType(value: String, type: Type): String {
        return when (String::class.java.typeName) {
            type.typeName -> {
                "\"${value}\""
            }
            else -> {
                value
            }
        }
    }

    fun createManifestPlaceholders(placeholders: MutableMap<String, Any>, reference: EnvironmentEntryExtension? = null) {
        if (reference != null) {
            reference.variables.forEach { v ->
                (variables[v.key] ?: v.value).run {
                    if (v.value.placeholder) {
                        placeholders[v.key] = value
                    }
                }
            }
        } else {
            variables.forEach { v ->
                if (v.value.placeholder) {
                    placeholders[v.key] = v.value.value
                }
            }
        }
    }

    fun asMap():Map<String,Any>{
        return variables.map { (key, value) ->
            key.toLowerCase() to value.value
        }.toMap()
    }
}