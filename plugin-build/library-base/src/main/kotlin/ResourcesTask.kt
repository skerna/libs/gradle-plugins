package io.skerna.libs.gradle.base

import io.skerna.libs.gradle.base.Constants.TASKS_GROUP
import io.skerna.libs.gradle.base.resources.PropertiesContext
import io.skerna.libs.gradle.base.resources.ResourceExtension
import io.skerna.libs.gradle.base.resources.ResourceTemplate
import com.github.mustachejava.DefaultMustacheFactory
import com.github.mustachejava.Mustache
import com.github.mustachejava.MustacheFactory
import io.skerna.libs.gradle.base.extensions.EnvironmentExtension
import io.skerna.libs.gradle.base.extensions.BaseExtension
import org.gradle.api.DefaultTask
import org.gradle.api.logging.Logging
import org.gradle.api.tasks.TaskAction
import java.io.*
import java.net.URISyntaxException
import java.nio.file.Path
import java.nio.file.Paths

open class ResourcesTask : DefaultTask() {
    private val contextReader = PropertiesContext()

    @TaskAction
    fun compileTemplate() {
        LOG.debug("Compile template execution runs")
        var baseExtension: BaseExtension? =
            getProject().getExtensions().getByType(BaseExtension::class.java)

        if(baseExtension==null){
            System.err.println("Arch extension not found in task ResourceTask")
            return
        }

        var resourceExtension = baseExtension.resourceExtension
        var environmentExtension: EnvironmentExtension? = getProject().getExtensions().getByType(EnvironmentExtension::class.java)


        if (resourceExtension == null) {
            LOG.info("No configuration found. Using default parameters.")
            resourceExtension = ResourceExtension(project)
        }
        var rootContext: Any? = resourceExtension.inputDirectory
        var resourceTemplates = mutableSetOf<ResourceTemplate>()

        val inDirectory = resourceExtension.inputDirectory.toString()
        val outDirectory = resourceExtension.outputDirectory
        val env = environmentExtension!!.currentEnvironment()
        resourceTemplates.addAll(resourceExtension.getResourceDefinitions())
        resourceTemplates.add(ResourceTemplate(inDirectory, outDirectory, env!!.asMap()))
        val templateDefinitionsAmount: Int = resourceTemplates.size
        if (templateDefinitionsAmount == 0) {
            LOG.warn("No template definitions found")
        } else {
            LOG.info("Template definitions found: {}", templateDefinitionsAmount)
        }

        for (templateDefinition in resourceTemplates) {
            val reader: Reader? = null
            var templateContext: Any
            //String sourceUri = templateDefinition.getSource();
            templateContext = if (templateDefinition.contextMap != null) {
                LOG.error("Lookup for context map first")
                templateDefinition.contextMap!!
            } else if (templateDefinition.context == null) {
                LOG.error("Lookup for parent context  file")
                if (rootContext == null) {
                    LOG.error("Context not defined")
                    throw RuntimeException("Context not defined")
                }
                LOG.debug("Using root context for template {}", templateDefinition.source)
                rootContext
            } else {
                LOG.error("Lookup for local context  file")
                LOG.debug("Using local context for template {}", templateDefinition.source)
                createContext(Paths.get(templateDefinition.context))
            }
            if (templateDefinition.isSourceDirectory) {
                try {
                    renderFromDirectory(templateDefinition, templateContext)
                } catch (e: URISyntaxException) {
                    e.printStackTrace()
                }
            } else {
                renderTemplate(templateDefinition, templateContext)
            }
        }
    }

    @Throws(URISyntaxException::class)
    private fun renderFromDirectory(templateTemplate: ResourceTemplate, rootContext: Any) {
        val directorySource: Path = Paths.get(templateTemplate.source)
        LOG.info("Render from directory $directorySource")
        val directoryOutput: Path = Paths.get(templateTemplate.target)
        LOG.info("Target render directory $directoryOutput")
        val existPath = directorySource.toFile().exists()
        if (!existPath) {
            LOG.error("Render target directory not exist $directorySource")
            throw RuntimeException("Render target directory not exist $directorySource")
        }
        val files: Set<File> = getProject().fileTree(directorySource).getFiles()
        for (file in files) {
            LOG.info("RENDER FILE $file")
            val filePath = file.toPath()
            val filePathRelative = directorySource.relativize(filePath)
            // Extract relative path
            // append relative file path to out dir
            val pathCompiledPath = directoryOutput.resolve(filePathRelative)
            var pathCompiledFile = pathCompiledPath.toFile()
            // IF extenssion is setting use same from origin
            if (templateTemplate.extension != null) {
                pathCompiledFile = changeExtension(pathCompiledFile, templateTemplate.extension!!)
            }
            val fileTarget = pathCompiledFile.toURI()
            val fileTemplateDef = ResourceTemplate(file.path, pathCompiledFile.path)
            renderTemplate(fileTemplateDef, rootContext)
        }
    }

    /**
     * Render single template using root context
     *
     * @param templateTemplate File
     * @param rootContext        Object
     */
    private fun renderTemplate(templateTemplate: ResourceTemplate, rootContext: Any) {
        var reader: Reader? = null
        LOG.info(">> RENDER TEMPLATE WITH {}", rootContext)
        try {
            reader = BufferedReader(FileReader(templateTemplate.source))
            val mustache: Mustache = mustacheFactory.compile(reader, "template")
            LOG.info("Save content template into {}", templateTemplate.target)
            val fileSvg: File = File(templateTemplate.target)
            fileSvg.parentFile.mkdirs()
            mustache.execute(FileWriter(fileSvg), rootContext).flush()
        } catch (e: Exception) {
            LOG.error("Error during load file")
            throw RuntimeException(e)
        } finally {
            reader?.close()
        }
    }

    private fun createContext(contextConfiguration: Path): Any {
        return contextReader.createContext(contextConfiguration)
    }

    companion object {
        private const val OUT_DIR_NAME = "mustache"
        private val LOG = Logging.getLogger(
            ResourcesTask::class.java
        )
        private val mustacheFactory: MustacheFactory = DefaultMustacheFactory()
        fun changeExtension(f: File, newExtension: String): File {
            val i = f.name.lastIndexOf('.')
            val name = f.name.substring(0, i)
            return File(f.parent + "/" + name + "." + newExtension)
        }
    }

    init {
        setGroup(TASKS_GROUP)
    }
}