package io.skerna.libs.gradle.base

import io.skerna.libs.gradle.base.extensions.EnvironmentEntryExtension
import io.skerna.libs.gradle.base.extensions.EnvironmentExtension
import org.gradle.api.Action
import org.gradle.api.Project
import java.nio.file.Path
import java.nio.file.Paths

/**
 * arch-gradle
 * @author ronald
 **/
fun Project.configOutDirectory(): String {
    return "${project.buildDir}/config"
}

fun Project.configOutDirectoryPath(): Path {
    return Paths.get(configOutDirectory())
}

fun Project.configDirectory(): String {
    return "${project.projectDir}/config"
}

fun Project.configDirectoryPath(): Path {
    return Paths.get(configDirectory())
}

fun Project.usinCurrentEnv(action: Action<EnvironmentEntryExtension>) {
    val archExtension = project.extensions.getByType(EnvironmentExtension::class.java)
    val envs = archExtension.currentEnvironment()
    project.afterEvaluate{
        envs?.let { it1 -> action.execute(it1) }
    }
}