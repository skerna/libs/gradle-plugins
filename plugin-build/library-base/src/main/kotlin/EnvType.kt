package io.skerna.libs.gradle.base

enum class EnvType(private val type: String) {
    DEV("dev"), TEST("test"), DEMO("demo"), RELEASE("release");

    override fun toString(): String {
        return type
    }
}