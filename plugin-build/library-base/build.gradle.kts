plugins {
    kotlin("jvm")
    id("java-gradle-plugin")
    id("com.gradle.plugin-publish")
    id("maven-publish")
    id("com.jfrog.bintray")
}

dependencies {
    implementation(kotlin("stdlib-jdk7"))
    implementation(gradleApi())
    implementation("com.jfrog.bintray.gradle:gradle-bintray-plugin:1.8.4")
    implementation("com.github.spullara.mustache.java:compiler:0.9.6")
    implementation("org.yaml:snakeyaml:1.24")
    testImplementation(TestingLib.JUNIT)
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

gradlePlugin {
    plugins {
        create(PluginBaseCoordinates.ID) {
            id = PluginBaseCoordinates.ID
            implementationClass = PluginBaseCoordinates.IMPLEMENTATION_CLASS
            version = PluginBaseCoordinates.VERSION
        }
    }
}
val PluginBundle = PluginBaseCoordinates.PluginBundle
// Configuration Block for the Plugin Marker artifact on Plugin Central
pluginBundle {
    website = PluginBundle.WEBSITE
    vcsUrl = PluginBundle.VCS
    description = PluginBundle.DESCRIPTION
    tags = PluginBundle.TAGS

    plugins {
        getByName(PluginBaseCoordinates.ID) {
            displayName = PluginBundle.DISPLAY_NAME
        }
    }
}

tasks.create("setupPluginUploadFromEnvironment") {
    doLast {
        val key = System.getenv("GRADLE_PUBLISH_KEY")
        val secret = System.getenv("GRADLE_PUBLISH_SECRET")

        if (key == null || secret == null) {
            throw GradleException("gradlePublishKey and/or gradlePublishSecret are not defined environment variables")
        }

        System.setProperty("gradle.publish.key", key)
        System.setProperty("gradle.publish.secret", secret)
    }
}

val publicationName = "lib"

publishing {
    val sourceJar = project.tasks.create("source", Jar::class.java).apply {
        archiveClassifier.set("sources")
        from(sourceSets.getByName("main").allSource)
    }
    publications.invoke {
        val publication: MavenPublication = create(publicationName, MavenPublication::class.java)

        publication.run{
            groupId = project.group.toString()
            artifactId = project.name
            version = project.version.toString()
            artifact(sourceJar)
        }
    }
}

fun findProperty(s: String) = project.findProperty(s) as String?
private val Project.bintray: com.jfrog.bintray.gradle.BintrayExtension
    get() = extensions.findByName("bintray") as? com.jfrog.bintray.gradle.BintrayExtension ?: error("Not found bvintray extensoins $name")

bintray {
    user = findProperty("BINTRAY_USER")
    key = findProperty("BINTRAY_KEY")
    publish = true
    setPublications(publicationName)
    pkg.run {
        repo = "maven"
        name = project.name
        userOrg = "skerna"
        override = true
        dryRun = false
        websiteUrl = "https://gitlab.com/skerna/libs/gradle-plugins"
        vcsUrl = "https://gitlab.com/skerna/libs/gradle-plugins"
        description = "Gradle plugins for skerna devs"
        setLabels("kotlin")
        setLicenses("MIT")
        desc = description
    }
}
