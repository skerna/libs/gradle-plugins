package io.skerna.mlibs.gradle

import io.skerna.libs.gradle.base.SkernaPluginBase
import org.gradle.api.Action
import org.gradle.api.Project

abstract class MultiplatformPlugin : SkernaPluginBase() {
    private val Project.android: com.android.build.gradle.BaseExtension
        get() = extensions.findByName("android") as? com.android.build.gradle.BaseExtension
            ?: error("Not android module $name")

    override fun configureMainExtension(): Class<out MultiplatformExtension> {
        return MultiplatformExtension::class.java
    }

    override fun configurePlugin(project: Project) {
        val multiplatformExtension: Class<out MultiplatformExtension> = this@MultiplatformPlugin.configureMainExtension()
        val extension = project.extensions.findByType(multiplatformExtension)
        if (extension != null) {
            super.configurePlugin(project)
            project.plugins.run {
                apply("org.jetbrains.kotlin.multiplatform")
                apply("maven-publish")
                if (extension.multiplatform.enableAndroid) {
                    project.plugins.run {
                        apply("com.android.library")
                    }
                    with(project) {
                        androidConfig()
                    }
                }
            }
        }

    }


    private fun Project.androidConfig() {
        android.run {
            compileSdkVersion(29)
            defaultConfig(Action {
                it.setMinSdkVersion(21)
                it.setTargetSdkVersion(29)
                it.setVersionCode(1)
                it.setVersionName("1.0")
                it.setTestInstrumentationRunner("android.support.test.runner.AndroidJUnitRunner")
            })
            buildTypes(Action {
                it.getByName("release") {
                    it.setMinifyEnabled(false)
                    it.setProguardFiles(
                        arrayListOf(
                            getDefaultProguardFile("proguard-android.txt"),
                            "proguard-rules.pro"
                        )
                    )
                }
            })
            packagingOptions(Action {
                it.exclude("META-INF/*.kotlin_module")
                it.exclude("io/ktor/http/cio/internals/*.kotlin_metadata")
                it.exclude("io/ktor/client/features/websocket/*.kotlin_metadata")
                it.exclude("io/ktor/http/cio/websocket/*.kotlin_metadata")
                it.exclude("io/ktor/http/cio/*.kotlin_metadata")
            })
            sourceSets.getByName("main") {
                it.manifest.srcFile("src/androidMain/AndroidManifest.xml")
                it.java.srcDir("src/androidMain/kotlin")
                it.res.srcDir("src/androidMain/res")
            }
            sourceSets.getByName("androidTest") {
                it.manifest.srcFile("src/androidMain/AndroidManifest.xml")
                it.java.srcDir("src/androidTest/kotlin")
                it.res.srcDir("src/androidTest/res")
            }
        }
    }
}
