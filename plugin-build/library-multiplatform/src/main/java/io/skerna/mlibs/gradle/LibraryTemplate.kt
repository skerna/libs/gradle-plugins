package io.skerna.mlibs.gradle

import org.gradle.api.DefaultTask
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.options.Option

abstract class LibraryTemplate : DefaultTask() {

    init {
        description = "Basic multiplatfrom template"
    }

    @get:Input
    @get:Option(option = "enableAndroid", description = "active android on multiplatform lib")
    @get:Optional
    abstract val enableAndroid: Property<Boolean>

    @get:Input
    @get:Option(option = "tag", description = "A Tag to be used for debug and in the output file")
    @get:Optional
    abstract val author: Property<String>

    @TaskAction
    fun sampleAction() {
        val prettyTag = author.orNull?.let { "[$it]" } ?: ""

        logger.lifecycle("$prettyTag Author is: ${author.orNull}")
        logger.lifecycle("$prettyTag Android is: ${enableAndroid.orNull}")
        logger.lifecycle("$prettyTag tag is: ${author.orNull}")
    }
}
