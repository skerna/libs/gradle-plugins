package io.skerna.mlibs.gradle

import io.skerna.libs.gradle.base.extensions.BaseExtension
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import javax.inject.Inject

@Suppress("UnnecessaryAbstractClass")
abstract class MultiplatformExtension @Inject constructor(project: Project): BaseExtension(project) {

    internal val multiplatform = Multiplatform(project)

    fun multiplatform(action: Action<Multiplatform>){
        action.execute(multiplatform)
    }
    @Suppress("UnnecessaryAbstractClass")
    class Multiplatform @Inject constructor(project: Project) {
        private val objects = project.objects
        var enableAndroid: Boolean = false


        override fun toString(): String {
            return "Multiplatform(" +
                    "objects=$objects, " +
                    "enableAndroid=$enableAndroid" +
                    ")"
        }

    }

}
